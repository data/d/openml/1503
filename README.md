# OpenML dataset: spoken-arabic-digit

https://www.openml.org/d/1503

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Data Collected by the Laboratory of Automatic and Signals, University of Badji-Mokhtar Annaba, Algeria.    
**Source**: UCI    
**Please cite**:   


* Title of Database: Spoken Arabic Digit

* Abstract: 

This dataset contains time series of mel-frequency cepstrum coefficients (MFCCs) corresponding to spoken Arabic digits. Includes data from 44 males and 44 females native Arabic speakers.

* Source:

Data Collected by the Laboratory of Automatic and Signals, 
University of Badji-Mokhtar 
Annaba, Algeria. 

Direction: Prof.Mouldi Bedda 
Participants: H.Dahmani, C.Snani, MC.Amara Korba, S.Atoui 
Adapted and preprocessed by : 
Nacereddine Hammami and Mouldi Bedda 
Faculty of Engineering, 
Al-Jouf University 
Sakaka, Al-Jouf 
Kingdom of Saudi Arabia 
e-mail: nacereddine.hammami '@' gmail.com 
mouldi_bedda '@' yahoo.fr 
Date: October, 2008


* Data Set Information:

Dataset from 8800 (10 digits x 10 repetitions x 88 speakers) time series of 13 Frequency Cepstral 
Coefficients (MFCCs) had taken from 44 males and 44 females Arabic native speakers 
between the ages 18 and 40 to represent ten spoken Arabic digit.


* Attribute Information:

Each line on the data base represents 13 MFCCs coefficients in the increasing order separated by spaces. This corresponds to one analysis frame. The 13 Mel Frequency Cepstral Coefficients (MFCCs) are computed with the following conditions; Sampling rate: 11025 Hz, 16 bits Window applied: hamming Filter pre-emphasized: 1-0.97Z^(-1)


* Relevant Papers:

[1] N. Hammami, M. Bedda ,"Improved Tree model for Arabic Speech Recognition", Proc. IEEE 
ICCSIT10 Conference, 2010. 
[2] N. Hammami, M. Sellami ,"Tree distribution classifier for automatic spoken Arabic digit 
recognition", Proc. IEEE ICITST09 Conference, 2009 , PP 1-4.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1503) of an [OpenML dataset](https://www.openml.org/d/1503). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1503/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1503/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1503/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

